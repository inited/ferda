
// nova verze pluginu iOS 10+
function notificationSetCustomInput() {

    try {
        // jedno maze uz spustena notifikace druhe ne
        cordova.plugins.notification.local.clearAll(function() {
            console.log("Upozornění vypnuto pres clearAll");
        }, this);
        cordova.plugins.notification.local.cancelAll(function() {
            console.log("Upozornění vypnuto pres cancelAll");
        }, this);
    } catch (e) {
        console.log(e);
    }

    var t = $("input[name='cas_notifikace']").val();
    if(t.length==0)
    {
        alert("Vložte prosím čas (HH:MM)");
        return;
    }
    var thours = t.split(":")[0];
    var tmins = t.split(":")[1];
    console.log(thours);
    console.log(tmins);

    try {
        // u trigger musi byt nastaven count, jinak se na androidu zacykli a spadne (ios ok),
        // count nesmi byt moc velky, jinak android -> chyba (out of memory),
        // 255 volani -> vola se jendou tydne, to je nejakych 5 let, a kdyz nastavi novy cas, tak se count nastavi
        // zase na 255, takze by melo stacit
        window.cordova.plugins.notification.local.schedule({
            title: '',
            text: 'Nezapomeň objednat pivo Ferdinand dnes do 17:00 hod',
            trigger: {every: {weekday: 2, hour: parseInt(thours),  minute: parseInt(tmins)}, count: 255},
            foreground: true
        });
    } catch (e) {
        console.log(e);
        alert("Upozornění se nepodařilo nastavit");
        return;
    }
    alert("Upozornění nastaveno");
}


// v pluginu blbne mazani, tak pro jistotu mazu vsechno
function notificationClear()
{
    try {
	// jedno maze uz spustena notifikace druhe ne
        cordova.plugins.notification.local.clearAll(function() {
            console.log("Upozornění vypnuto pres clearAll");
        }, this);
        cordova.plugins.notification.local.cancelAll(function() {
            console.log("Upozornění vypnuto pres cancelAll");
        }, this);
	alert("Upozornění vypnuto");
        return;
    } catch (e) {
        console.log(e);
    	alert("Notifikaci se nepodařilo zrušit.")
    }
}


function notificationTriggerSet()
{
    cordova.plugins.notification.local.on("click", function(notification) {

        if($("#CollapsiblePanel9 div.CollapsiblePanelContentM").css("display")=="none")
        {
            $("#CollapsiblePanel9 div.CollapsiblePanelTabM").click();
        }
        console.log("SCROLL");
        console.log($('#CollapsiblePanel9 div.CollapsiblePanelTabM').offset().top);
        $(window).scrollTop($('#CollapsiblePanel9 div.CollapsiblePanelTabM').offset().top);
    });
}
